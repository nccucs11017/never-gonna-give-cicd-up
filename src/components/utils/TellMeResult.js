/**
 * @param {string} guessed the possible answer
 * @param {string} expected the real answer of the quiz
 * @return {string} the result of this match
 */
export function calResult(guessed='', expected='') {
  if (Boolean(guessed) === false ||
      Boolean(expected) === false ||
      expected.length !== guessed.length) {
    return 'invalid';
  }

  let aNum = 0;
  let bNum = 0;
  const map = new Array(10).fill(0);
  let guessedElmt;
  let expectedElmt;
  for (let i = 0; i < expected.length; i++) {
    guessedElmt = Number(guessed[i]);
    expectedElmt = Number(expected[i]);

    if (guessedElmt === expectedElmt) aNum++;
    else {
      if (map[expectedElmt] < 0) bNum++;
      if (map[guessedElmt] > 0) bNum++;
      map[guessedElmt]--;
      map[expectedElmt]++;
    }
  }

  return aNum + 'A' + bNum + 'B';
}
